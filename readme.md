# SOA labs: 6. Authentication and Authorization with JWT
## Use JWT to authenticate and authorize microservices by using Login microservice

### Reproduce steps

1. Clone the project
   * `git clone git@gitlab.com:soa-labs/2018-5.git`

2. Build all docker images:
   * `./build-images.sh`

3. Start mysql (and wait for it to start):
   * `docker-compose up -d mysql`

4. Ensure mysql container is up and running:
   * `docker ps`
   * `docker logs <container-id>` 

5. Start other services (and wait for them to run):
   * `docker-compose up -d`
   * `docker ps` 

6. Access API Gateway aggregated greeting endpoint (you will be denied access):
   * `http://localhost/zuul/gateway/greeting`

6. Use Postman to get JWT token:
   * Make a HTTP Post to `http://localhost/zuul/login/login` with Json body { "username": "admin", "password": "admin" }
   * See the JWT token in the response header Authorization

6. Access API Gateway aggregated greeting endpoint with Postman
   * Make a HTTP Get to `http://localhost/zuul/gateway/greeting`
   * Set the JWT token from the previous step in Athorization header

9. Shutdown containers:
   * `docker-compose down`
